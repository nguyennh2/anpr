from glob import glob
from pathlib import Path

import numpy as np
import cv2
import cv_utils as cvu
import time
import tensorflow as tf
from os.path import splitext, basename
from inference.lpdetector.utils import getWH, nms

from inference.lpdetector.utils import getRectPts, find_T_matrix
from inference.lpdetector.label import Shape, writeShapes
from inference.lpdetector.utils import im2single
from inference.lpdetector.detection_query import DLabel


class LpDetectorService:

    def __init__(self, model_path):
        self.model_path = model_path
        self.model = self.load()

    def load(self) -> tf.keras.models.Model:
        gpus = tf.config.experimental.list_physical_devices('GPU')
        if gpus:
            # Restrict TensorFlow to only allocate 1GB of memory on the first GPU
            try:
                tf.config.experimental.set_virtual_device_configuration(
                    gpus[0],
                    [tf.config.experimental.VirtualDeviceConfiguration(memory_limit=1024 * 0.75)])
                logical_gpus = tf.config.experimental.list_logical_devices('GPU')
                print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
            except RuntimeError as e:
                # Virtual devices must be set before GPUs have been initialized
                print(e)
        start_time = time.time()
        path = splitext(self.model_path)[0]
        with open(f'{path}.json', 'r') as json_file:
            model_json = json_file.read()
        model = tf.keras.models.model_from_json(model_json)
        model.load_weights('%s.h5' % path)
        print(f"wpod-net loaded in --- {(time.time() - start_time)} seconds ---")

        return model

    def detect_lp(self, I, max_dim, net_step, out_size, threshold=0.5):
        min_dim_img = min(I.shape[:2])
        factor = float(max_dim) / min_dim_img

        w, h = (np.array(I.shape[1::-1], dtype=float) * factor).astype(int).tolist()
        w += (w % net_step != 0) * (net_step - w % net_step)
        h += (h % net_step != 0) * (net_step - h % net_step)
        Iresized = cv2.resize(I, (w, h))

        T = Iresized.copy()
        T = T.reshape((1, T.shape[0], T.shape[1], T.shape[2]))

        start = time.time()
        Yr = self.model.predict(T)
        Yr = np.squeeze(Yr)
        elapsed = time.time() - start

        L, TLps = self.reconstruct(I, Iresized, Yr, out_size, threshold)

        return L, TLps, elapsed

    def inference_img_array(self, img_data, lp_threshold):
        print('Processing image')
        ratio = float(max(img_data.shape[:2])) / min(img_data.shape[:2])
        side = int(ratio * 288.)
        bound_dim = min(side + (side % (2 ** 4)), 608)
        Llp, LlpImgs, elapsed = self.detect_lp(
            im2single(img_data),
            bound_dim,
            2 ** 4,
            (240, 80),
            lp_threshold
        )
        print(f'lp_dectector in {elapsed}')
        if len(LlpImgs):
            Ilp = LlpImgs[0]
            Ilp = cv2.cvtColor(Ilp, cv2.COLOR_BGR2GRAY)
            Ilp = cv2.cvtColor(Ilp, cv2.COLOR_GRAY2BGR)

            # s = Shape(Llp[0].pts)
            return Ilp * 255

    def inference_image_path(self, img_path, lp_threshold, output_dir, output_image_size=(240, 174)):
        print('\t Processing %s' % img_path)
        bname = splitext(basename(img_path))[0]
        Ivehicle = cv2.imread(img_path, 1)
        ilp, s = self.inference_image(Ivehicle, lp_threshold, output_image_size)
        cv2.imwrite('%s/%s_lp.png' % (output_dir, bname), ilp)
        writeShapes('%s/%s_lp.txt' % (output_dir, bname), [s])
        return ilp, s

    def inference_image(self, image: np.ndarray, lp_threshold, output_image_size=(240, 160)):
        ratio = float(max(image.shape[:2])) / min(image.shape[:2])
        side = int(ratio * 288.)
        bound_dim = min(side + (side % (2 ** 4)), 608)
        Llp, LlpImgs, elapsed = self.detect_lp(
            I=im2single(image),
            max_dim=bound_dim,
            net_step=2 ** 4,
            out_size=output_image_size,
            threshold=lp_threshold
        )
        print(f'lp_dectector in {elapsed}')
        ilp = []
        shape = None

        if len(LlpImgs):
            ilp = LlpImgs[0]
            ilp = cv2.cvtColor(ilp, cv2.COLOR_BGR2GRAY)
            ilp = cv2.cvtColor(ilp, cv2.COLOR_GRAY2BGR)

            shape = Shape(Llp[0].pts)

        return ilp * 255, shape

    def reconstruct(self, Iorig, I, Y, out_size, threshold=.9):

        net_stride = 2 ** 4
        side = ((208. + 40.) / 2.) / net_stride  # 7.75

        Probs = Y[..., 0]
        Affines = Y[..., 2:]
        rx, ry = Y.shape[:2]
        ywh = Y.shape[1::-1]
        iwh = np.array(I.shape[1::-1], dtype=float).reshape((2, 1))

        xx, yy = np.where(Probs > threshold)

        WH = getWH(I.shape)
        MN = WH / net_stride

        vxx = vyy = 0.5  # alpha

        base = lambda vx, vy: np.matrix([[-vx, -vy, 1.], [vx, -vy, 1.], [vx, vy, 1.], [-vx, vy, 1.]]).T
        labels = []

        for i in range(len(xx)):
            y, x = xx[i], yy[i]
            affine = Affines[y, x]
            prob = Probs[y, x]

            mn = np.array([float(x) + .5, float(y) + .5])

            A = np.reshape(affine, (2, 3))
            A[0, 0] = max(A[0, 0], 0.)
            A[1, 1] = max(A[1, 1], 0.)

            pts = np.array(A * base(vxx, vyy))  # *alpha
            pts_MN_center_mn = pts * side
            pts_MN = pts_MN_center_mn + mn.reshape((2, 1))

            pts_prop = pts_MN / MN.reshape((2, 1))

            labels.append(DLabel(0, pts_prop, prob))

        final_labels = nms(labels, .1)
        TLps = []

        if len(final_labels):
            final_labels.sort(key=lambda x: x.prob(), reverse=True)
            for i, label in enumerate(final_labels):
                t_ptsh = getRectPts(0, 0, out_size[0], out_size[1])
                ptsh = np.concatenate((label.pts * getWH(Iorig.shape).reshape((2, 1)), np.ones((1, 4))))
                H = find_T_matrix(ptsh, t_ptsh)
                Ilp = cv2.warpPerspective(Iorig, H, out_size, borderValue=.0)

                TLps.append(Ilp)

        return final_labels, TLps


if __name__ == "__main__":
    import config

    lp_detector = LpDetectorService(
        model_path='../../models/lp-detector/wpod-net_update1.h5'
    )
    input_dir = Path('../../samples/greenparking/Image')
    imgs_paths = glob('%s/*.jpg' % input_dir)
    print(imgs_paths)
    output_dir = input_dir / "lp_output"
    output_dir.mkdir(exist_ok=True)
    for i, img_path in enumerate(imgs_paths):
        print(f'processing {img_path}')
        _, _ = lp_detector.inference_image_path(img_path, lp_threshold=0.5, output_dir=output_dir)
