import numpy as np
import cv2
from inference.lpdetector.detection_query import DLabel


class Projection(DLabel):
    """ Support function for image processing """

    def im2single(self, Image):
        return Image.astype('float32') / 255.

    def getWH(self, shape):
        return np.array(shape[1::-1]).astype(float)

    """ Support function for NMS and IOU """

    def IOU(self, tl1, br1, tl2, br2):
        wh1, wh2 = br1 - tl1, br2 - tl2
        assert ((wh1 >= 0).all() and (wh2 >= 0).all())

        intersection_wh = np.maximum(np.minimum(br1, br2) - np.maximum(tl1, tl2), 0)
        intersection_area = np.prod(intersection_wh)
        area1, area2 = (np.prod(wh1), np.prod(wh2))
        union_area = area1 + area2 - intersection_area
        return intersection_area / union_area

    def IOU_labels(self, l1, l2):
        return self.IOU(l1.tl(), l1.br(), l2.tl(), l2.br())

    def nms(self, Labels, iou_threshold=0.5):
        SelectedLabels = []
        Labels.sort(key=lambda l: l.prob(), reverse=True)

        for label in Labels:
            non_overlap = True
            for sel_label in SelectedLabels:
                if self.IOU_labels(label, sel_label) > iou_threshold:
                    non_overlap = False
                    break

            if non_overlap:
                SelectedLabels.append(label)
        return SelectedLabels

    """ Projection util function"""

    def find_T_matrix(self, pts, t_pts):
        A = np.zeros((8, 9))
        for i in range(0, 4):
            xi = pts[:, i]
            xil = t_pts[:, i]
            xi = xi.T

            A[i * 2, 3:6] = -xil[2] * xi
            A[i * 2, 6:] = xil[1] * xi
            A[i * 2 + 1, :3] = xil[2] * xi
            A[i * 2 + 1, 6:] = -xil[0] * xi

        [U, S, V] = np.linalg.svd(A)
        H = V[-1, :].reshape((3, 3))
        return H

    def getRectPts(self, tlx, tly, brx, bry):
        return np.matrix([[tlx, brx, brx, tlx], [tly, tly, bry, bry], [1, 1, 1, 1]], dtype=float)

    def normal(self, pts, side, mn, MN):
        pts_MN_center_mn = pts * side
        pts_MN = pts_MN_center_mn + mn.reshape((2, 1))
        pts_prop = pts_MN / MN.reshape((2, 1))
        return pts_prop

    def reconstruct(self, I, Iresized, Yr, lp_threshold):
        # 4 max-pooling layers, stride = 2
        net_stride = 2 ** 4
        side = ((208 + 40) / 2) / net_stride

        # one line and two lines license plate size
        one_line = (470, 110)
        two_lines = (280, 200)

        Probs = Yr[..., 0]
        Affines = Yr[..., 2:]

        xx, yy = np.where(Probs > lp_threshold)
        # CNN input image size
        WH = self.getWH(Iresized.shape)
        # output feature map size
        MN = WH / net_stride

        vxx = vyy = 0.5  # alpha
        base = lambda vx, vy: np.matrix([[-vx, -vy, 1], [vx, -vy, 1], [vx, vy, 1], [-vx, vy, 1]]).T
        labels = []
        labels_frontal = []

        for i in range(len(xx)):
            x, y = xx[i], yy[i]
            affine = Affines[x, y]
            prob = Probs[x, y]

            mn = np.array([float(y) + 0.5, float(x) + 0.5])

            # affine transformation matrix
            A = np.reshape(affine, (2, 3))
            A[0, 0] = max(A[0, 0], 0)
            A[1, 1] = max(A[1, 1], 0)
            # identity transformation
            B = np.zeros((2, 3))
            B[0, 0] = max(A[0, 0], 0)
            B[1, 1] = max(A[1, 1], 0)

            pts = np.array(A * base(vxx, vyy))
            pts_frontal = np.array(B * base(vxx, vyy))

            pts_prop = self.normal(pts, side, mn, MN)
            frontal = self.normal(pts_frontal, side, mn, MN)

            labels.append(DLabel(0, pts_prop, prob))
            labels_frontal.append(DLabel(0, frontal, prob))

        final_labels = self.nms(labels, 0.1)
        final_labels_frontal = self.nms(labels_frontal, 0.1)

        if final_labels_frontal:
            # LP size and type
            out_size, lp_type = (two_lines, 2) if (
                    (final_labels_frontal[0].wh()[0] / final_labels_frontal[0].wh()[1]) < 1.7) else (one_line, 1)
        else:
            out_size, lp_type = (one_line, 1)

        TLp = []
        if len(final_labels):
            final_labels.sort(key=lambda x: x.prob(), reverse=True)
            for _, label in enumerate(final_labels):
                t_ptsh = self.getRectPts(0, 0, out_size[0], out_size[1])
                ptsh = np.concatenate((label.pts * self.getWH(I.shape).reshape((2, 1)), np.ones((1, 4))))
                H = self.find_T_matrix(ptsh, t_ptsh)

                Ilp = cv2.warpPerspective(I, H, out_size, borderValue=0)
                TLp.append(Ilp)

        return final_labels, TLp, lp_type
