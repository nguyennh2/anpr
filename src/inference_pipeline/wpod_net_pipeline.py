import sys
import traceback
from io import BytesIO
import cv_utils as cvu
import config
import time
import re
import cv2
import numpy as np
import requests
from glob import glob
from pathlib import Path
from PIL import Image
from OCR_model.darknet_window_GPU.darknet import detect_ocr
import OCR_model.darknet_window_GPU.darknet as dn
from utils import inference_utils
from utils import predefine_path
import os

from inference.lpdetector.lp_detector_service import LpDetectorService
from inference.yolo.yolov3_inference_engine import InferenceEngine


class WpodnetPipeline:
    lp_ocr: InferenceEngine
    lp_detector: LpDetectorService
    lp_threshold: float
    output_dir: Path

    def __init__(self, lp_detector, lp_ocr, lp_threshold=0.5, output_dir=Path("output")):
        self.lp_detector = lp_detector
        self.lp_ocr = lp_ocr
        self.lp_threshold = lp_threshold
        self.output_dir = output_dir
        self.ocr_net = dn.load_net(predefine_path.OCR_MODEL_CONFIG, predefine_path.OCR_MODEL_WEIGHT, 0)
        self.ocr_meta = dn.load_meta(predefine_path.OCR_MODEL_DATASET)

    def inference_image_path(self, image_path: Path) -> (str, np.ndarray):
        image = cv2.imread(str(image_path))
        return self.inference_image_api(image)

    def inference_image(self, image) -> (str, np.ndarray):
        image, _ = self.lp_detector.inference_image(image, self.lp_threshold, output_image_size=(240, 240))
        # convert to u-int
        image = image.astype(np.uint8)
        # shape[0] = y (height)
        first_line_cropped = image[0:int(image.shape[0] / 2), 0:image.shape[1]]
        second_line_cropped = image[int(image.shape[0] / 2) + 1:image.shape[0], 0:image.shape[1]]

        first_line_str, im_1 = self.perform_single_line_ocr(first_line_cropped)
        second_line_str, im_2 = self.perform_single_line_ocr(second_line_cropped)

        str_result = first_line_str + second_line_str
        im_result = np.concatenate((im_1, im_2), axis=0)

        return str_result, im_result

        # plate_number = self.ocrPredict(image)
        # return plate_number, image

    def inference_image_api(self, image) -> str:
        image, _ = self.lp_detector.inference_image(image, self.lp_threshold, output_image_size=(240, 162))
        # image, _ = self.lp_detector.inference_image(image, self.lp_threshold)
        # convert to u-int
        image = image.astype(np.uint8)
        # shape[0] = y (height)
        # first_line_cropped = image[0:int(image.shape[0] / 2), 0:image.shape[1]]
        # second_line_cropped = image[int(image.shape[0] / 2) + 1:image.shape[0], 0:image.shape[1]]
        #
        # first_line_str = self.plate_reader(first_line_cropped)
        # time.sleep(0.1)
        # second_line_str = self.plate_reader(second_line_cropped)

        str_results = self.plate_reader(image)
        str_result = ''.join(str_results)
        pattern = re.compile('[\W_]+')
        str_result = pattern.sub('', str_result)
        # im_result = np.concatenate((im_1, im_2), axis=0)

        return str_result

    def plate_reader(self, image) -> [str]:
        cv2.imwrite("warpimage.resized.jpg", image)
        ret, jpeg = cv2.imencode(".jpg", image)
        files = {
            'image': ('image.jpg', jpeg.tobytes()),
            'language': (None, 'kr'),
        }

        response = requests.post('https://demo.ocr.clova.ai/api/api/request', files=files)
        if response.ok and response.json()["words"]:
            result = [item["text"].upper() for item in response.json()["words"]]
        else:
            result = [str(response.status_code)]
        print(f'API result {result}')
        return result

    def perform_single_line_ocr(self, image):
        bbox, ocr_result_im = self.lp_ocr.run(
            input_image=image, draw_boxes=True
        )
        # cvu.show_image(ocr_result_im, "cropped")
        ocr_text = self.get_ocr_result(bbox, image.shape[0], squared_lp=False)
        return ocr_text, ocr_result_im

    # def detect_characters_in_set_image(self, detectImage, ocr_threshold):
    #     plate_number = ''
    #     for image in detectImage:
    #         if image.shape[0] > 2 and image.shape[1] > 2:
    #             cv2.imwrite('temporary_licenseplate.png', cv2.cvtColor(image, cv2.COLOR_RGB2BGR))
    #             im = dn.load_image('temporary_licenseplate.png'.encode('utf-8'), 0, 0)
    #             dn.rgbgr_image(im)
    #             R, (width, height) = detect_ocr(self.ocr_net, self.ocr_meta, im, thresh=ocr_threshold, nms=None)
    #             plate_number += inference_utils.postProcessFinalLicensePlateString(R, height, width)
    #             os.remove('temporary_licenseplate.png')
    #     return plate_number
    #
    # def ocrPredict(self, licenseplateImage):
    #     plate_number = ''
    #     if licenseplateImage is not None:
    #         ocr_threshold = .45
    #         if (licenseplateImage.shape[1] * 1.0 / licenseplateImage.shape[0]) < 1.88:
    #             detectImage = inference_utils.cropImageToTwoPart(licenseplateImage)
    #         else:
    #             detectImage = [licenseplateImage]
    #         plate_number = self.detect_characters_in_set_image(detectImage, ocr_threshold)
    #     return plate_number

    def get_ocr_result(self, darknet_response: [dict], image_height: int, squared_lp=True) -> str:
        bboxes_data = darknet_response['bounding-boxes']
        sorted_bboxes = sorted(bboxes_data, key=lambda i: i['coordinates']['left'])
        result = [item['ObjectClassName'] for item in sorted_bboxes]
        if not squared_lp:
            return ''.join(result)

        # Square lp case:
        mid_line = int(image_height / 2)
        first_line_character = []
        second_line_character = []

        for item in sorted_bboxes:
            character_center_y = item['coordinates']['center'][1]
            character_class_name = item['ObjectClassName']
            if character_center_y < mid_line:
                first_line_character += [character_class_name]
                continue

            second_line_character += [character_class_name]

        return ''.join(first_line_character) + ''.join(second_line_character)
