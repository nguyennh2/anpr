from pathlib import Path

MODELS_DIR = Path("models")
WPODNET_WEIGHT = MODELS_DIR / "lp-detector" / "wpod-net_update1.h5"
YOLO_BASE_DIR = Path('models/yolo')
BASE_YOLO_INFERENCE = Path('/home/nguyen/anpr/src/inference/yolo')
