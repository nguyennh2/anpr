import sys
import traceback
from glob import glob
from pathlib import Path
import cv_utils as cvu
import config
from inference.lpdetector.lp_detector_service import LpDetectorService
from inference.yolo.yolo_inference_service import YoloInferenceService

from inference_pipeline.wpod_net_pipeline import WpodnetPipeline

sys.path.append('inference/yolo')

if __name__ == "__main__":
    try:
        input_dir = Path("samples/greenparking/Image")
        imgs_paths = glob('%s/*.jpg' % input_dir)
        print(imgs_paths)
        output_dir = input_dir / "output"
        output_dir.mkdir(exist_ok=True)
        wpodnet = WpodnetPipeline(
            lp_detector=LpDetectorService(config.WPODNET_WEIGHT),
            lp_ocr=YoloInferenceService().get_model('ocr'),
            lp_threshold=.5,
            output_dir=output_dir
        )

        for i, img_path in enumerate(imgs_paths):
            ocr_text, processed_lp_image = wpodnet.inference_image_path(img_path)
            print(ocr_text)
            # cvu.show_image(processed_lp_image)
            break

    except:
        traceback.print_exc()
        sys.exit(1)

    sys.exit(0)
