import cv2
import numpy as np


def show_image(img: np.ndarray, window_name="image"):
    cv2.namedWindow(window_name, cv2.WINDOW_GUI_EXPANDED)
    cv2.imshow(window_name, cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
    cv2.waitKey(0)
    cv2.destroyAllWindows()
