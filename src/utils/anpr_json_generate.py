import json
from utils.information_query import JsonInformation


class ANPRJSonTemplate(JsonInformation):

    def __init__(self):
        super().__init__()
        self.detection = {}
        self.anpr_data = []
        self.anpr_data_reduced = []

    def create_anpr_json_reduced(self):
        self.anpr_data_reduced = json.dumps({
            "timeStampMillis": self.timestamp(),
            "category": {
                "id": self.cat_id(),
                "name": self.cat_name()
            },
            "criticality": self.critic(),
            "numberPlate": self.numberplate(),
            "confidence": self.confidence(),
            "location": {
                "type": "Point",
                "coordinates": [
                    self.loc_longitude(),
                    self.loc_latitude(),
                    self.loc_altitude()
                ]
            },
            "originator": {
                "camera": {
                    "channel": self.cam_chan(),
                    "name": self.cam_name(),
                    "type": self.cam_type()
                },
                "analysis": {
                    "channel": self.ana_ch(),
                    "name": self.ana_name(),
                    "hostname": self.ana_hostname()
                }
            },
            "images": [{
                "url": self.media_img_url(),
                "content": "image",
                "wrapRect": {
                    "top": self.img_warp()['top'],
                    "bottom": self.img_warp()['bottom'],
                    "left": self.img_warp()['left'],
                    "right": self.img_warp()['right']
                }
            },
                {
                    "url": self.media_patch_url(),
                    "content": "patch"
                },
                {
                    "url": self.media_overview_url(),
                    "content": "overview"
                }
            ],
        })
        return self.anpr_data_reduced

    def create_common_json(self):
        self.anpr_data = json.dumps({
            "timeStampMillis": self.timestamp(),
            "category": {
                "id": self.cat_id(),
                "name": self.cat_name()
            },
            "criticality": self.critic(),
            "numberPlate": self.numberplate(),
            "country": self.country(),
            "confidence": self.confidence(),
            "location": {
                "type": "Point",
                "coordinates": [
                    self.loc_longitude(),
                    self.loc_latitude(),
                    self.loc_altitude()
                ]
            },
            "originator": {
                "type": self.ori_type(),
                "ip": self.cam_ip(),
                "camera": {
                    "channel": self.cam_chan(),
                    "name": self.cam_name(),
                    "type": self.cam_type()
                },
                "analysis": {
                    "channel": self.ana_ch(),
                    "name": self.ana_name(),
                    "hostname": self.ana_hostname()
                }
            },
            "images": [{
                "url": self.media_img_url(),
                "content": "image",
                "wrapRect": {
                    "top": self.img_warp()['top'],
                    "bottom": self.img_warp()['bottom'],
                    "left": self.img_warp()['left'],
                    "right": self.img_warp()['right']
                }
            },
                {
                    "url": self.media_patch_url(),
                    "content": "patch"
                },
                {
                    "url": self.media_overview_url(),
                    "content": "overview"
                }
            ],
            "extraData": [{
                    "key": self.key1(),
                    "value": self.value1()
                },
                {
                    "key": self.key2(),
                    "value": self.value2()
                },
                {
                    "key": self.key3(),
                    "value": self.value3()
                }
            ]
        })
        return self.anpr_data
