import numpy as np


class JsonInformation:

    def __init__(self):
        self.__timestamp = None

        self.__numberplate = None
        self.__confidence = None
        self.__country = None
        self.__critic = None
        self.__cat_id = None
        self.__cat_name = None

        self.__ori_type = None
        self.__loc_altitude = None
        self.__loc_latitude = None
        self.__loc_longitude = None

        self.__cam_type = None
        self.__cam_name = None
        self.__cam_chan = None
        self.__cam_ip = None

        self.__ana_hostname = None
        self.__ana_name = None
        self.__ana_ch = None

        self.__media_overview_url = None
        self.__media_patch_url = None
        self.__media_img_url = None

        self.__img_warp = {"top": None, "bottom": None, "left": None, "right": None}

        self.__key1 = None
        self.__value1 = None
        self.__key2 = None
        self.__value2 = None
        self.__key3 = None
        self.__value3 = None

    def reset(self):
        self.__timestamp = None

        self.__numberplate = None
        self.__confidence = None
        self.__country = None

        self.__critic = None
        self.__cat_id = None
        self.__cat_name = None

        self.__ori_type = None
        self.__loc_altitude = None
        self.__loc_latitude = None
        self.__loc_longitude = None

        self.__cam_type = None
        self.__cam_name = None
        self.__cam_chan = None
        self.__cam_ip = None

        self.__ana_hostname = None
        self.__ana_name = None
        self.__ana_ch = None

        self.__media_overview_url = None
        self.__media_patch_url = None
        self.__media_img_url = None

        self.__img_warp = {"top": None, "bottom": None, "left": None, "right": None}

        self.__key1 = None
        self.__value1 = None
        self.__key2 = None
        self.__value2 = None
        self.__key3 = None
        self.__value3 = None

    def timestamp(self): return self.__timestamp

    def numberplate(self): return self.__numberplate

    def confidence(self): return self.__confidence

    def country(self): return self.__country

    def cat_id(self): return self.__cat_id

    def cat_name(self): return self.__cat_name

    def critic(self): return self.__critic

    def loc_longitude(self): return self.__loc_longitude

    def loc_latitude(self): return self.__loc_latitude

    def loc_altitude(self): return self.__loc_altitude

    def ori_type(self): return self.__ori_type

    def cam_ip(self): return self.__cam_ip

    def cam_chan(self): return self.__cam_chan

    def cam_name(self): return self.__cam_name

    def cam_type(self): return self.__cam_type

    def ana_ch(self): return self.__ana_ch

    def ana_name(self): return self.__ana_name

    def ana_hostname(self): return self.__ana_hostname

    def media_img_url(self): return self.__media_img_url

    def media_patch_url(self): return self.__media_patch_url

    def media_overview_url(self): return self.__media_overview_url

    def img_warp(self): return self.__img_warp

    def key1(self): return self.__key1

    def key2(self): return self.__key2

    def key3(self): return self.__key3

    def value1(self): return self.__value1

    def value2(self): return self.__value2

    def value3(self): return self.__value3

    def set_timestamp(self, timestamp):
        self.__timestamp = timestamp

    def set_numberplate(self, numberplate):
        self.__numberplate = numberplate

    def set_confidence(self, confidence):
        self.__confidence = confidence

    def set_country(self, country):
        self.__country = country

    def set_cat_id(self, cat_id):
        self.__cat_id = cat_id

    def set_cat_name(self, cat_name):
        self.__cat_name = cat_name

    def set_critic(self, critic):
        self.__critic = critic

    def set_loc_longitude(self, longitude):
        self.__loc_longitude = longitude

    def set_loc_latitude(self, latitude):
        self.__loc_latitude = latitude

    def set_loc_altitude(self, altitude):
        self.__loc_altitude = altitude

    def set_ori_type(self, ori_type):
        self.__ori_type = ori_type

    def set_cam_ip(self, cam_ip):
        self.__cam_ip = cam_ip

    def set_cam_chan(self, cam_chan):
        self.__cam_chan = cam_chan

    def set_cam_name(self, cam_name):
        self.__cam_name = cam_name

    def set_cam_type(self, cam_type):
        self.__cam_type = cam_type

    def set_ana_ch(self, ana_ch):
        self.__ana_ch = ana_ch

    def set_ana_name(self, ana_name):
        self.__ana_name = ana_name

    def set_ana_hostname(self, ana_hostname):
        self.__ana_hostname = ana_hostname

    def set_media_img_url(self, media_img_url):
        self.__media_img_url = media_img_url

    def set_media_patch_url(self, media_patch_url):
        self.__media_patch_url = media_patch_url

    def set_media_overview_url(self, media_overview_url):
        self.__media_overview_url = media_overview_url

    def set_img_warp(self, img_warp):
        self.__img_warp = img_warp

    def set_key1(self, key):
        self.__key1 = key

    def set_key2(self, key):
        self.__key2 = key

    def set_key3(self, key):
        self.__key3 = key

    def set_value1(self, value):
        self.__value1 = value

    def set_value2(self, value):
        self.__value2 = value

    def set_value3(self, value):
        self.__value3 = value
