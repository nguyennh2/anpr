import numpy as np
import cv2
import torch
import OCR_model.darknet_window_GPU.darknet as dn

from imutils import perspective
from OCR_model.projection_utils import Projection
from scipy.spatial.distance import cdist


def convertNumpyToTorchTensor(numpy_image, device, normalize=True):
    numpy_image = np.expand_dims(numpy_image.transpose(2, 0, 1), 0)
    torch_image = torch.from_numpy(numpy_image).float().to(device)
    if normalize:
        torch_image = torch_image / 255.0
    return torch_image


def convertTorchTensorToNumpy(torch_tensor):
    numpy_array = torch_tensor[0, :, :, :].data.cpu().numpy()
    numpy_array = numpy_array.transpose(1, 2, 0)
    return numpy_array


def convertBatchTorchTensorToNumpy(torch_tensor):
    numpy_array = torch_tensor[:, :, :, :].data.cpu().numpy()
    numpy_array = numpy_array.transpose(0, 2, 3, 1)
    return numpy_array


def convertNumpyToDarknetImage(arr):
    arr = (arr.transpose(2, 0, 1)).astype(np.float32)
    c = arr.shape[0]
    h = arr.shape[1]
    w = arr.shape[2]
    arr = (arr / 255.0).flatten()
    data = dn.c_array(dn.c_float, arr)
    im = dn.IMAGE(w, h, c, data)
    return im


def cropImageToTwoPart(image):
    height, width = image.shape[:2]
    start_row, start_col = int(0), int(0)
    end_row, end_col = int(height * .55), int(width)
    cropped_top = image[start_row:end_row, start_col:end_col]
    start_row, start_col = int(height * .45), int(0)
    end_row, end_col = int(height), int(width)
    cropped_bot = image[start_row:end_row, start_col:end_col]
    return [cropped_top, cropped_bot]


def cropImageFromContour(input_image, contour):
    (x, y, w, h) = cv2.boundingRect(contour)
    (x, y, w, h) = x - 8, y - 8, w + 16, h + 16
    topleft_x = np.clip(x, 0, input_image.shape[1] - 1)
    topleft_y = np.clip(y, 0, input_image.shape[0] - 1)
    bottomright_x = np.clip(topleft_x + w, 0, input_image.shape[1] - 1)
    bottomright_y = np.clip(topleft_y + h, 0, input_image.shape[0] - 1)
    return input_image[topleft_y:bottomright_y, topleft_x:bottomright_x], {'top': y,
                                                                           'bottom': input_image.shape[0] - y - h,
                                                                           'left': x,
                                                                           'right': input_image.shape[1] - x - w}


def reorderPointPositions(pts):
    rect = np.zeros((4, 2), dtype="float32")
    s = pts.sum(axis=1)
    rect[0] = pts[np.argmin(s)]
    rect[2] = pts[np.argmax(s)]
    diff = np.diff(pts, axis=1)
    rect[1] = pts[np.argmin(diff)]
    rect[3] = pts[np.argmax(diff)]
    return rect


def four_point_PerspectiveTransform(image, pts):
    rect = reorderPointPositions(pts)
    (tl, tr, br, bl) = rect
    widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
    widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
    maxWidth = max(int(widthA), int(widthB))
    heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
    heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
    maxHeight = max(int(heightA), int(heightB))

    if maxHeight > 0:
        if (maxWidth * 1.0 / maxHeight) > 5:
            return None
        elif (maxWidth * 1.0 / maxHeight) < 1.8:
            maxWidth = 280
            maxHeight = 200
        else:
            maxWidth = 470
            maxHeight = 110

    dst = np.array([
        [0, 0],
        [maxWidth - 1, 0],
        [maxWidth - 1, maxHeight - 1],
        [0, maxHeight - 1]], dtype="float32")
    M = cv2.getPerspectiveTransform(rect, dst)
    warped_image = cv2.warpPerspective(image, M, (maxWidth, maxHeight))
    return warped_image


def warpLicensePlateBasedImageProbability(image, probability_map):
    warp_image = None
    eroded_probability_image = cv2.erode(probability_map, None, iterations=2)
    image_binary = preProcessingEnhanceImage(image.copy())
    filtered_edged = cv2.dilate(
        (cv2.Canny((image_binary * 1.0 / 255.0).astype(np.uint8) * eroded_probability_image, 10, 25)), None,
        iterations=4)
    cnts, _ = cv2.findContours(filtered_edged.astype(np.uint8), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    if len(cnts) != 0:
        cnt = sorted(cnts, key=cv2.contourArea, reverse=True)[0]
        peri = cv2.arcLength(cnt, True)
        approximate_polygon = cv2.approxPolyDP(cnt, 0.000001 * peri, True)
        rect = cv2.minAreaRect(cnt)
        box = cv2.boxPoints(rect)
        box = np.array(box, dtype="int")
        box = perspective.order_points(box)
        distance_result = cdist(approximate_polygon.reshape(approximate_polygon.shape[0], 2), box, 'euclidean')
        index_maxvalue = np.argmin(distance_result, axis=0)
        processed_polygon = approximate_polygon[index_maxvalue]
        warp_image = four_point_PerspectiveTransform(image, processed_polygon.reshape((4, 2)))
    return warp_image


def preProcessingEnhanceImage(input_image):
    lab_image = cv2.cvtColor(input_image, cv2.COLOR_BGR2Lab)
    equalized_image = cv2.equalizeHist(lab_image[:, :, 0])
    equalized_image = cv2.GaussianBlur(equalized_image, (5, 5), 0.1)
    _, output_image = cv2.threshold(equalized_image, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    return output_image


def correctCharacterToNumber(character):
    if character == "L" or character == "A":
        correct_character = "4"
    elif character == "I":
        correct_character = "1"
    elif character in ["Q", "D", "C", "G", "U", "H"]:
        correct_character = "0"
    elif character == "Z":
        correct_character = "2"
    elif character == "B":
        correct_character = "8"
    elif character in ["S", "J"]:
        correct_character = "5"
    else:
        correct_character = character
    return correct_character


def correctNumberToCharacter(number):
    if number == "0":
        correct_number = "Q"
    elif number == "1":
        correct_number = "L"
    elif number in ["2", "7"]:
        correct_number = "Z"
    elif number == "4":
        correct_number = "A"
    elif number == "5":
        correct_number = "S"
    elif number == "8":
        correct_number = "B"
    # elif number == "W":
    #     correct_number = "N"
    # elif number == "I":
    #     correct_number = "L"
    else:
        correct_number = number
    return correct_number


def correctCharacterToNumberCharacter4th(character):
    if character in ["L"]:
        correct_character = "4"
    elif character in ["Z"]:
        correct_character = "2"
    else:
        correct_character = character
    return correct_character


def process_number_plate_vn(number_plate):
    line = ''
    if 7 <= len(number_plate) <= 9:
        for i in range(len(number_plate)):
            if (i == 0 or i == 1) and number_plate[i].isnumeric() is False:
                line += correctCharacterToNumber(number_plate[i])
            elif i == 2 and number_plate[i].isnumeric() is True:
                line += correctNumberToCharacter(number_plate[i])
            elif i in [len(number_plate) - 3, len(number_plate) - 2, len(number_plate) - 1] \
                    and number_plate[i].isnumeric() is False:
                line += correctCharacterToNumber(number_plate[i])
            else:
                line += number_plate[i]
    return line


def process_number_plate_uk(number_plate):
    return number_plate
    # TODO
    line = ""
    return line


def postProcessFinalLicensePlateString(R, height, width):
    plate_number = ''
    if len(R):
        L = Projection().dknet_label_conversion(R, width, height)
        L = Projection().nms(L, .45)
        L.sort(key=lambda x: x.tl()[0])
        lp_str = ''.join([chr(l.cl()) for l in L])
        plate_number += str(lp_str)
    return plate_number


def postProcessBestLicensePlate(bestLicensePlates):
    if bestLicensePlates is not None:
        for licensePlateResult in bestLicensePlates:
            if licensePlateResult != '':
                print(licensePlateResult)


def WPODNETInference(model, input_image, max_dim, lp_threshold):
    input_image_inference = Projection().im2single(input_image.copy())
    min_dim_img = min(input_image_inference.shape[:2])
    factor = float(max_dim) / min_dim_img
    ImgWidth, ImgHeight = (np.array(input_image_inference.shape[1::-1], dtype=float) * factor).astype(int).tolist()
    Iresized = cv2.resize(input_image_inference, (ImgWidth, ImgHeight))
    input_image_inference = Iresized.copy()
    input_image_inference = input_image_inference.reshape((1, input_image_inference.shape[0],
                                                           input_image_inference.shape[1],
                                                           input_image_inference.shape[2]))
    predict_result = np.squeeze(model.predict(input_image_inference))
    bb_box_results, warp_images, lp_types = Projection().reconstruct(input_image, Iresized, predict_result,
                                                                     lp_threshold)
    return bb_box_results, warp_images, lp_types


def transfromWPODNETBoundingBoxToImageCoordinate(imageWidthHeight, bbox_info):
    pts = bbox_info.pts * imageWidthHeight
    xmin = int(min(pts[:, 0].T[0], pts[:, 3].T[0]))  # Compare top-left and bot-left
    xmax = int(max(pts[:, 1].T[0], pts[:, 2].T[0]))  # Compare top-right and bot-right
    ymin = int(min(pts[:, 0].T[1], pts[:, 1].T[1]))  # Compare top-left and top-right
    ymax = int(max(pts[:, 3].T[1], pts[:, 2].T[1]))  # Compare bot-left and bot-right
    return {'top': ymin, 'bottom': ymax, 'left': xmin, 'right': xmax}

# def loadKerasModelFromJson(path):
#     path = splitext(path)[0]
#     with open('%s.json' % path, 'r') as json_file:
#         model_json = json_file.read()
#     model = model_from_json(model_json, custom_objects={})
#     model.load_weights('%s.h5' % path)
#     return model
