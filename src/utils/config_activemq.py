import os
import json
import cv2
import stomp
import requests
import base64

from utils.anpr_json_generate import ANPRJSonTemplate

URL_MEDIA_STORE = "https://asa-vn.dallmeier.vn/media-storage/rest/media"
HEADERS = {'content-type': 'image/jpeg'}

ACTIVEMQ_USER = 'admin'
ACTIVEMQ_PASSWORD = 'admin'
ACTIVEMQ_HOST = '203.205.43.68'
ACTIVEMQ_PORT = 61613
TOPIC_ACTIVEMQ_ANPR = '/topic/VirtualTopic.eep.event.anpr'
# TOPIC_ACTIVEMQ_ANPR = '/topic/parkingsite.eep.input.anpr'

USERNAME = os.getenv("ACTIVEMQ_USER") or ACTIVEMQ_USER
PASSWORD = os.getenv("ACTIVEMQ_PASSWORD") or ACTIVEMQ_PASSWORD
HOST = os.getenv("ACTIVEMQ_HOST") or ACTIVEMQ_HOST
PORT = os.getenv("ACTIVEMQ_PORT") or ACTIVEMQ_PORT


class ActiveMQService(object):
    def __init__(self):
        self.connection = stomp.Connection(host_and_ports=[(HOST, PORT)], auto_content_length=True)
        self.connection.connect(login=USERNAME, passcode=PASSWORD)
        self.technical_user = base64.b64encode(b"asa:7Fwzlf1ISjvwBTu2m6Xc").decode("ascii")
        self.anpr_json_template = ANPRJSonTemplate()

    def generateAndSendJSONToTopic(self, license_plate_number, overviewImage, patchImage, licensePlateImage, confidence, warp_position):
        response_media_overview = self.send_image_to_media_storage(overviewImage)
        response_media_patch = self.send_image_to_media_storage(patchImage)
        response_media_image = self.send_image_to_media_storage(licensePlateImage)
        confidence = str(int(confidence*100))
        json_activemq = self.generate_json_activemq(response_media_overview, response_media_patch, response_media_image,
                                                    license_plate_number, confidence, warp_position)
        self.connection.send(TOPIC_ACTIVEMQ_ANPR, json_activemq)
        self.anpr_json_template.reset()

    def generate_json_activemq(self, response_media_overview, response_media_patch, response_media_image, number_plate_value,
                               confidence, warp_positions):
        self.define_default()
        self.anpr_json_template.set_img_warp(warp_positions)
        self.anpr_json_template.set_numberplate(number_plate_value)
        self.anpr_json_template.set_confidence(confidence)
        self.anpr_json_template.set_timestamp(response_media_image["uploadedAt"])
        self.anpr_json_template.set_media_img_url(URL_MEDIA_STORE + '/' + response_media_image["id"])
        self.anpr_json_template.set_media_overview_url(URL_MEDIA_STORE + '/' + response_media_overview["id"])
        self.anpr_json_template.set_media_patch_url(URL_MEDIA_STORE + '/' + response_media_patch["id"])
        self.anpr_json_template.create_common_json()
        self.anpr_json_template.anpr_data = json.loads(self.anpr_json_template.create_common_json())
        self.anpr_json_template.anpr_data['images'] = [elem for elem in self.anpr_json_template.anpr_data['images'] if
                                              elem['url'] is not None]
        return json.dumps(self.anpr_json_template.anpr_data)

    def send_image_to_media_storage(self, frame_storage_media_copy):
        _, img_encoded_jpg = cv2.imencode('.jpg', frame_storage_media_copy)
        img_transfer_byte = img_encoded_jpg.tobytes()
        response_media = self.save_image_to_media_storage(URL_MEDIA_STORE, img_transfer_byte)
        response_media = json.loads(response_media)
        return response_media

    def save_image_to_media_storage(self, url_media, img):
        files = {'data': ('', img, 'image/jpeg')}
        headers_media = {'authorization': "Basic %s" % self.technical_user, 'cache-control': "no-cache"}
        response_media = requests.post(url=url_media, headers=headers_media, files=files)
        return response_media.text

    def define_default(self):
        self.anpr_json_template.set_country("VietNam")
        self.anpr_json_template.set_critic(10)
        self.anpr_json_template.set_cat_id(1)
        self.anpr_json_template.set_cat_name("Unknown")
        self.anpr_json_template.set_loc_longitude(12.0953035354614)
        self.anpr_json_template.set_loc_latitude(49.0123546160334)
        self.anpr_json_template.set_loc_altitude(52.0)
        self.anpr_json_template.set_ori_type("anprBackendOriginator")
        self.anpr_json_template.set_cam_ip("192.168.1.1")
        self.anpr_json_template.set_cam_chan("1")
        self.anpr_json_template.set_cam_name("Camera 1")
        self.anpr_json_template.set_cam_type("IN")
        self.anpr_json_template.set_ana_ch("1")
        self.anpr_json_template.set_ana_name("DINPR Germany 50")
        self.anpr_json_template.set_ana_hostname("DAO9-00142313")
        self.anpr_json_template.set_img_warp({"top": "50", "bottom": "50", "left": "50", "right": "50"})
        self.anpr_json_template.set_key1("group_or_comment")
        self.anpr_json_template.set_value1("This is a comment")
        self.anpr_json_template.set_key2("info1")
        self.anpr_json_template.set_value2("this is the first info")
        self.anpr_json_template.set_key3("info2")
        self.anpr_json_template.set_value3("this is the second info")