import sys
import traceback
import cv2
import numpy as np
import os
from glob import glob
from pathlib import Path
from inference.yolo.yolo_inference_service import YoloInferenceService

sys.path.append('inference/yolo')


def show_image(img: np.ndarray):
    cv2.imshow("output", img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


if __name__ == "__main__":
    try:
        yolo_service = YoloInferenceService()
        ocr_model = yolo_service.get_model('ocr')

        input_dir = Path("samples/single-image/output")
        imgs_paths = glob('%s/*.png' % input_dir)
        print(imgs_paths)
        output_dir = input_dir / "ocr_output"
        output_dir.mkdir(exist_ok=True)

        lp_threshold = .5
        for i, img_path in enumerate(imgs_paths):
            input_ocr_image = cv2.imread(img_path)
            bbox, output_im = ocr_model.run(
                input_image=input_ocr_image, draw_boxes=True
            )
            print(bbox)
            show_image(output_im)

    except:
        traceback.print_exc()
        sys.exit(1)

    sys.exit(0)
