import csv
import cv2
import time
import cv_utils as cvu
from tqdm import tqdm
from pathlib import Path

import config
from inference.lpdetector.lp_detector_service import LpDetectorService
from inference.yolo.yolo_inference_service import YoloInferenceService
from inference_pipeline.wpod_net_pipeline import WpodnetPipeline


class ModelEvaluation:
    def __init__(self, path: Path, inference_pipeline):
        self.ds_path = path
        self.images_path = path / "Image"
        self.label_path = path / "label.csv"
        self.anpr_pipeline = inference_pipeline

    def get_images_path(self):
        return self.images_path

    def get_label_path(self):
        return self.label_path

    def get_label_dict(self):
        with self.label_path.open() as infile:
            reader = csv.reader(infile)
            dict_label = {rows[0]: rows[1] for rows in reader}
        return dict_label

    def get_model_accuracy(self):
        label_dict = self.get_label_dict()  # warning mutable dict, remove the first element of dict
        label_dict.pop('image_path')
        correct_count = 0
        rows = []
        label: str
        label_dict = list(label_dict.items())
        sample_count = len(label_dict)
        for i in tqdm(range(0, sample_count)):
            time.sleep(2)
            file_name = label_dict[i][0]
            label = label_dict[i][1]
            image_path = self.images_path / file_name
            image = cv2.imread(str(image_path))
            try:
                lp_results = self.anpr_pipeline.inference_image_api(image)
                lp_results_str = lp_results.upper()
                lp_label_str = label.replace('\n', '').replace(',', '').upper()
                correct_prediction = lp_results_str == lp_label_str
                rows += [[file_name, label, lp_results_str, int(correct_prediction)]]
                if correct_prediction:
                    correct_count += 1
                    print(f"correct case {correct_count} / {sample_count}")
                else:
                    # cvu.show_image(lp_ocr_output_im)
                    print(f'Incorrect: pipeline result {lp_results_str} vs. real {lp_label_str}')
            except RuntimeError:
                rows += [[file_name, label, 'exception', 0]]
                continue

        accuracy = str(round(correct_count / (len(label_dict) - 1), 3))
        print(f'Accuracy: {accuracy}')

        csv_file = open('evaluation.csv', 'w')
        with csv_file:
            writer = csv.writer(csv_file)
            writer.writerows(rows)


if __name__ == "__main__":
    ds_path = Path.cwd() / "dataset/greenparking/"
    output_dir = ds_path / "output"
    output_dir.mkdir(exist_ok=True)
    pipeline = WpodnetPipeline(
        lp_detector=LpDetectorService(config.WPODNET_WEIGHT),
        lp_ocr=YoloInferenceService().get_model('ocr'),
        lp_threshold=.5,
        output_dir=output_dir
    )
    evaluation = ModelEvaluation(ds_path, pipeline)
    evaluation.get_model_accuracy()
